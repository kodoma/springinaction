package com.myapp.config;

import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * MyServletInitializer class.
 * Created on 25.02.2019.
 * @author Sokolov2-DO.
 */
// Если необходимо зарегистрировать любые дополнительные компоненты в веб-контейнере,
// нужно создать новый класс инициализатора
public class MyServletInitializer implements WebApplicationInitializer {

    // MyServletInitializer является довольно простым классом инициализатора для регистрации сервлетов.
    // Он регистрирует сервлет и сопоставляет его с одним путем "/custom/**"
    @Override
    public void onStartup(ServletContext servletContext) {
        // Зарегистрировать сервлет
        final ServletRegistration.Dynamic myServlet = servletContext.addServlet("myServlet", MyServlet.class);

        // Добавить маппинг сервлета
        myServlet.addMapping("/custom/**");
    }

    /*
     * Вы можете использовать этот подход для регистрации DispatcherServlet вручную.
     * Но в этом нет необходимости, потому что AbstractAnnotationConfigDispatcherServletInitializer
     * отлично работает без большого количества кода.
     */

/*    @Override
    public void onStartup(ServletContext servletContext) {
        // Зарегистрировать фильтр
        final FilterRegistration.Dynamic filter = servletContext.addFilter("myFilter", MyFilter.class);

        // Добавить фильтр
        filter.addMappingForUrlPatterns(null, false, "/custom/*");
    }*/
}
