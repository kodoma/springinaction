package com.myapp.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

/**
 * MyWebAppInitializer class.
 * Created on 25.02.2019.
 * @author Sokolov2-DO.
 */
// AbstractAnnotationConfigDispatcherServletInitializer автоматически регистрирует DispatcherServlet и ContextLoaderListener
public class MyWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[0];
    }

    @Override
    protected String[] getServletMappings() {
        return new String[0];
    }

    // Чтобы зарегистрировать один или несколько фильтров и сопоставить их с DispatcherServlet,
    // все что вам нужно нужно - это переопределить метод getServletFilters()
    @Override
    protected Filter[] getServletFilters() {
        // Любой фильтр, возвращаемый из getServletFilters() будет автоматически сопоставлен с DispatcherServlet
        return new Filter[] {new MyFilter()};
    }
}
