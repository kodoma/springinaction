package com.springinaction.aop.concert;

/**
 * CriticismEngine class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
public interface CriticismEngine {

    String getCriticism();
}
