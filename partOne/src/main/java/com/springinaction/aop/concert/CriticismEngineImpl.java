package com.springinaction.aop.concert;

/**
 * CriticismEngineImpl class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
public class CriticismEngineImpl implements CriticismEngine {

    public CriticismEngineImpl() {}

    // injected
    private String[] criticismPool;

    @Override
    public String getCriticism() {
        final int i = (int)(Math.random() * criticismPool.length);
        return criticismPool[i];
    }

    public void setCriticismPool(String[] criticismPool) {
        this.criticismPool = criticismPool;
    }
}
