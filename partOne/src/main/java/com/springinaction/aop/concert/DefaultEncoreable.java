package com.springinaction.aop.concert;

/**
 * DefaultEncoreable class.
 * Created on 21.01.2019.
 * @author Kodoma.
 */
public class DefaultEncoreable implements Encoreable {

    @Override
    public void performEncore() {
        System.out.println("Encore!");
    }
}
