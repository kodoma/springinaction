package com.springinaction.aop.concert;

/**
 * Encoreable class.
 * Created on 21.01.2019.
 * @author Kodoma.
 */
public interface Encoreable {

    void performEncore();
}
