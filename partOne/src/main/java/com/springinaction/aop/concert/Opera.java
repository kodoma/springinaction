package com.springinaction.aop.concert;

/**
 * Opera class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
public class Opera implements Performance {

    @Override
    public void perform() {
        System.out.println("Opera in action...");
    }
}
