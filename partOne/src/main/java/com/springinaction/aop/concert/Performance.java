package com.springinaction.aop.concert;

/**
 * Performance class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
public interface Performance {

    void perform();
}
