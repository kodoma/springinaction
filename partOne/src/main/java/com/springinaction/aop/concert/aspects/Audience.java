package com.springinaction.aop.concert.aspects;

import org.aspectj.lang.annotation.*;

/**
 * Audience class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
@Aspect
// Помечает класс как аспект
public class Audience {

    @Pointcut("execution(* com.springinaction.aop.concert.Performance.perform(..))")
    // Определяем точку внедрения аспекта
    public void performance() {
    }

    @Before("performance()")
    // Метод advice вызывается перед вызовом рекомендованного метода
    // Ссылаемся на объявленную выше точку внедрения
    public void silenceCellPhones() {
        System.out.println("Silencing cell phones");
    }

    @Before("performance()")
    public void takeSeats() {
        System.out.println("Taking seats");
    }

    @AfterReturning("performance()")
    // Метод advice вызывается после возврата рекомендованного метода
    // (сработает только при успешном выполнении метода)
    public void applause() {
        System.out.println("CLAP CLAP CLAP!!!");
    }

    @AfterThrowing("performance()")
    // Метод advice вызывается после того, как рекомендованный метод выбросит исключение
    public void demandRefund() {
        System.out.println("Demanding a refund");
    }
}
