package com.springinaction.aop.concert.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * AudienceAround class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
@Aspect
public class AudienceAround {

    @Pointcut("execution(* com.springinaction.aop.concert.Performance.perform(..))")
    public void performance() {
    }

    @Around("performance()")
    // Эта аннотация аналогична @Before, @AfterReturning, @AfterThrowing,
    // но здесь все собрано в одном месте
    public void watchPerformance(ProceedingJoinPoint jp) {
        try {
            System.out.println("Silencing cell phones");
            System.out.println("Taking seats");
            jp.proceed();
            System.out.println("CLAP CLAP CLAP!!!");
        } catch (Throwable e) {
            System.out.println("Demanding a refund");
        }
    }
}
