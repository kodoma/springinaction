package com.springinaction.aop.concert.aspects;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * AudienceAroundXml class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
public class AudienceAroundXml {

    public void watchPerformance(ProceedingJoinPoint jp) {
        try {
            System.out.println("Silencing cell phones");
            System.out.println("Taking seats");
            jp.proceed();
            System.out.println("CLAP CLAP CLAP!!!");
        } catch (Throwable e) {
            System.out.println("Demanding a refund");
        }
    }
}
