package com.springinaction.aop.concert.aspects;

/**
 * AudienceXML class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
public class AudienceXML {

    public void silenceCellPhones() {
        System.out.println("Silencing cell phones");
    }

    public void takeSeats() {
        System.out.println("Taking seats");
    }

    public void applause() {
        System.out.println("CLAP CLAP CLAP!!!");
    }

    public void demandRefund() {
        System.out.println("Demanding a refund");
    }
}
