package com.springinaction.aop.concert.aspects;

import com.springinaction.aop.concert.CriticismEngine;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Pointcut;

/**
 * CriticAspect class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
public class CriticAspect {

    private CriticismEngine criticismEngine;

    public CriticAspect() {}

    @Pointcut("execution(* com.springinaction.aop.concert.Performance.perform(..))")
    public void performance() {
    }

    @AfterReturning("performance()")
    public void afterReturning() {
        System.out.println(criticismEngine.getCriticism());
    }

    public void setCriticismEngine(CriticismEngine criticismEngine) {
        this.criticismEngine = criticismEngine;
    }
}
