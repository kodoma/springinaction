package com.springinaction.aop.concert.aspects;

import com.springinaction.aop.concert.DefaultEncoreable;
import com.springinaction.aop.concert.Encoreable;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;

/**
 * EncoreableIntroducer class.
 * Created on 21.01.2019.
 * @author Kodoma.
 */
@Aspect
// Когда Spring обнаруживает аннотированный бин с @Aspect, он автоматически создает прокси,
// который делегирует вызовы либо прокси-компоненту, либо реализации, в зависимости от того,
// к кому принадлежит вызываемый метод, прокси-компоненту или введенному интерфейсу
public class EncoreableIntroducer {

    @DeclareParents(value="com.springinaction.aop.concert+", defaultImpl= DefaultEncoreable.class)
    // Value определяет виды бинов, которые должны быть введены в интерфейс
    // '+' означает любой подтип
    // Определяет класс, реализующий интерфейс
    public static Encoreable encoreable;
}
