package com.springinaction.aop.concert.config;

import com.springinaction.aop.concert.Opera;
import com.springinaction.aop.concert.Performance;
import com.springinaction.aop.concert.aspects.AudienceAround;
import com.springinaction.aop.concert.aspects.EncoreableIntroducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * ConcertConfig class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
@Configuration
@ComponentScan
@EnableAspectJAutoProxy
// Включает автопроксирование. В Spring аспекты подключаются через Proxy
public class ConcertConfig {

    @Bean
    public Performance performance() {
        return new Opera();
    }

    @Bean
    public AudienceAround audience() {
        return new AudienceAround();
    }

    @Bean
    public EncoreableIntroducer encoreableIntroducer() {
        return new EncoreableIntroducer();
    }
}
