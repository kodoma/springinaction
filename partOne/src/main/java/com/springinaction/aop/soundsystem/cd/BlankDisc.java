package com.springinaction.aop.soundsystem.cd;

import java.util.List;

/**
 * BlankDisc class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankDisc implements CompactDisc {

    private String title;
    private String artist;
    private List<String> tracks;

    public BlankDisc() {
    }

    public BlankDisc(String title, String artist, List<String> tracks) {
        this.title = title;
        this.artist = artist;
        this.tracks = tracks;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setTracks(List<String> tracks) {
        this.tracks = tracks;
    }

    @Override
    public void playTrack(int trackNumber) {
        System.out.println("Playing " + title + " by " + artist);
        System.out.println("-Track: " + (trackNumber > tracks.size() ? null : tracks.get(trackNumber)));
    }
}
