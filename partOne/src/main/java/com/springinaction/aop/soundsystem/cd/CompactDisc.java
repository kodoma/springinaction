package com.springinaction.aop.soundsystem.cd;

/**
 * CompactDisc class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
public interface CompactDisc {

    void playTrack(int trackNumber);
}
