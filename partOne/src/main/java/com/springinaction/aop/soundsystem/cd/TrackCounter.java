package com.springinaction.aop.soundsystem.cd;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import java.util.HashMap;
import java.util.Map;

/**
 * TrackCounter class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
@Aspect
public class TrackCounter {

    private Map<Integer, Integer> trackCounts = new HashMap<>();

    @Pointcut("execution(* com.springinaction.aop.soundsystem.cd.CompactDisc.playTrack(..)) && args(trackNumber)")
    // Точка внедрения аспекта с аргументом
    public void trackPlayed(int trackNumber) {
    }

    @Before("trackPlayed(trackNumber)")
    public void countTrack(int trackNumber) {
        final int currentCount = getPlayCount(trackNumber);

        trackCounts.put(trackNumber, currentCount + 1);
    }

    public int getPlayCount(int trackNumber) {
        return trackCounts.getOrDefault(trackNumber, 0);
    }
}
