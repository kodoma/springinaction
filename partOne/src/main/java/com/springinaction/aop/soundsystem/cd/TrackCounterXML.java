package com.springinaction.aop.soundsystem.cd;

import java.util.HashMap;
import java.util.Map;

/**
 * TrackCounterXML class.
 * Created on 21.01.2019.
 * @author Kodoma.
 */
public class TrackCounterXML {

    private Map<Integer, Integer> trackCounts = new HashMap<>();

    public void countTrack(int trackNumber) {
        final int currentCount = getPlayCount(trackNumber);

        trackCounts.put(trackNumber, currentCount + 1);
    }

    public int getPlayCount(int trackNumber) {
        return trackCounts.getOrDefault(trackNumber, 0);
    }
}
