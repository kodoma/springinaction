package com.springinaction.aop.soundsystem.config;

import com.springinaction.aop.soundsystem.cd.BlankDisc;
import com.springinaction.aop.soundsystem.cd.CompactDisc;
import com.springinaction.aop.soundsystem.cd.TrackCounter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.ArrayList;
import java.util.List;

/**
 * TrackCounterConfig class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
@Configuration
@EnableAspectJAutoProxy
public class TrackCounterConfig {

    @Bean
    public CompactDisc sgtPeppers() {
        final BlankDisc cd = new BlankDisc();
        final List<String> tracks = new ArrayList<>();

        cd.setTitle("Sgt. Pepper's Lonely Hearts Club Band");
        cd.setArtist("The Beatles");

        tracks.add("Sgt. Pepper's Lonely Hearts Club Band");
        tracks.add("With a Little Help from My Friends");
        tracks.add("Lucy in the Sky with Diamonds");
        tracks.add("Getting Better");
        tracks.add("Fixing a Hole");

        cd.setTracks(tracks);

        return cd;
    }

    @Bean
    public TrackCounter trackCounter() {
        return new TrackCounter();
    }
}
