package com.springinaction.condition;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * MagicBeanConfig class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
@Configuration
@Conditional(MagicExistsCondition.class)
// Этот бин будет создан, только если метод matches() в MagicExistsCondition вернет true
public class MagicBeanConfig {

    @Bean
    public MagicBean magicBean() {
        return new MagicBean();
    }
}
