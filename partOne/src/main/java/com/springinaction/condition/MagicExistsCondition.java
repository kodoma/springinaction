package com.springinaction.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * MagicExistsCondition class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
public class MagicExistsCondition implements Condition {

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        final Environment env = conditionContext.getEnvironment();
        return env.containsProperty("magic");
    }
}
