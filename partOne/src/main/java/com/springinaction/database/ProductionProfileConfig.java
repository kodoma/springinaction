package com.springinaction.database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jndi.JndiObjectFactoryBean;

import javax.activation.DataSource;

/**
 * ProductionProfileConfig class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
@Configuration
// Эта аннотация сообщает Spring, что bean-компоненты в этом классе конфигурации должны быть созданы только если активен профиль разработчика.
// Если профиль разработчика не активен, то метод @Bean будет игнорироваться
public class ProductionProfileConfig {

    @Bean
    @Profile("dev")
    // База данных инициализируется в зависимости от текущего активного профиля
    public javax.sql.DataSource embeddedDataSource() {
        return new EmbeddedDatabaseBuilder()
                       .setType(EmbeddedDatabaseType.H2)
                       .addScript("classpath:schema.sql")
                       .addScript("classpath:test-data.sql")
                       .build();
    }

    @Bean
    @Profile("prod")
    public DataSource jndiDataSource() {
        final JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();

        jndiObjectFactoryBean.setJndiName("jdbc/myDS");
        jndiObjectFactoryBean.setResourceRef(true);
        jndiObjectFactoryBean.setProxyInterface(javax.sql.DataSource.class);

        return (DataSource)jndiObjectFactoryBean.getObject();
    }
}
