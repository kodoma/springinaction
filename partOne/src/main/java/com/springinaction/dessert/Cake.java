package com.springinaction.dessert;

import org.springframework.stereotype.Component;

/**
 * Cake class.
 * Created on 16.01.2019.
 * @author Kodoma.
 */
@Component
public class Cake implements Dessert {
}
