package com.springinaction.dessert;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * DessertConfig class.
 * Created on 16.01.2019.
 * @author Kodoma.
 */
@Configuration
@ComponentScan(basePackages = "com.springinaction.dessert")
public class DessertAutoConfig {
}
