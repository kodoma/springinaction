package com.springinaction.dessert;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * DessertManualConfig class.
 * Created on 16.01.2019.
 * @author Kodoma.
 */
@Configuration
public class DessertManualConfig {

    @Bean
    public Dessert cake() {
        return new Cake();
    }

    @Bean
    public Dessert cookies() {
        return new Cookies();
    }

    @Bean
    @Qualifier("cold")
    // То же самое, что и для @Component
    public Dessert iceCream() {
        return new IceCream();
    }

    @Bean
    public Dish dish() {
        return new Dish();
    }
}
