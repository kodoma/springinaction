package com.springinaction.dessert;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Dish class.
 * Created on 16.01.2019.
 * @author Kodoma.
 */
@Component
public class Dish {

    private Dessert dessert;

    @Autowired
    @Qualifier("cold")
    public void setDessert(Dessert dessert) {
        this.dessert = dessert;
    }

    public Dessert getDessert() {
        return dessert;
    }
}
