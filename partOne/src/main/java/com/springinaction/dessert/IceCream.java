package com.springinaction.dessert;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * IceCream class.
 * Created on 16.01.2019.
 * @author Kodoma.
 */
@Component
@Primary
// Если возникает неоднозначность при автозаполнении, эта аннотация говорит Spring,
// что этот бин имеет приоритет при выборе из бинов с таким же классом.
// Однако, если 2 и более бинов имею аннотацию @Primary, Spring так же не сможет выбрать бин для автозаполнения
@Qualifier("cold")
// Квалификатор и id бина - это 2 разные вещи, для автозаполнения лучше использовать @Qualifier, т.к. в этом случае
// мы можем изменять имя класса как угодно, и при этом автозаполнение не сломается
public class IceCream implements Dessert {
}
