package com.springinaction.knights;

import com.springinaction.knights.knight.Knight;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * KnightMain class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public class KnightMain {

    public static void main(String[] args) {
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/knights/knights.xml");
        //final ApplicationContext context = new AnnotationConfigApplicationContext(com.springinaction.knights.config.KnightConfig.class);
        final Knight knight = context.getBean(Knight.class);

        knight.embarkOnQuest();

        context.close();
    }
}
