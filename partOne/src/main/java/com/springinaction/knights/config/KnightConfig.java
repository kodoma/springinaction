package com.springinaction.knights.config;

import com.springinaction.knights.Minstrel;
import com.springinaction.knights.knight.Knight;
import com.springinaction.knights.di.BraveKnight;
import com.springinaction.knights.quests.Quest;
import com.springinaction.knights.quests.SlayDragonQuest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * KnightConfig class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
@Configuration
public class KnightConfig {

    @Bean
    public Knight knight() {
        return new BraveKnight(quest(), minstrel());
    }

    @Bean
    public Quest quest() {
        return new SlayDragonQuest(System.out);
    }

    @Bean
    public Minstrel minstrel() {
        return new Minstrel(System.out);
    }
}
