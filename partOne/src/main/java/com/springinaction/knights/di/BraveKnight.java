package com.springinaction.knights.di;

import com.springinaction.knights.Minstrel;
import com.springinaction.knights.knight.Knight;
import com.springinaction.knights.quests.Quest;

/**
 * BraveKnight class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public class BraveKnight implements Knight {

    private Quest quest;
    private Minstrel minstrel;

    public BraveKnight(Quest quest, Minstrel minstrel) {
        this.quest = quest;
        this.minstrel = minstrel;
    }

    public void embarkOnQuest() {
        minstrel.singBeforeQuest();
        quest.embark();
        minstrel.singAfterQuest();
    }
}
