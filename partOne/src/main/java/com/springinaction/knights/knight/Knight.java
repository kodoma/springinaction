package com.springinaction.knights.knight;

/**
 * Knight class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public interface Knight {

    void embarkOnQuest();
}
