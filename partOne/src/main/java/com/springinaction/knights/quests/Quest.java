package com.springinaction.knights.quests;

/**
 * Quest class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public interface Quest {

    void embark();
}
