package com.springinaction.knights.quests;

import java.io.PrintStream;

/**
 * SlayDragonQuest class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public class SlayDragonQuest implements Quest {

    private PrintStream stream;

    public SlayDragonQuest(PrintStream stream) {
        this.stream = stream;
    }

    public void embark() {
        stream.println("Embarking on quest to slay the dragon!");
    }
}
