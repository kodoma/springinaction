package com.springinaction.knights.simple;

import com.springinaction.knights.knight.Knight;

/**
 * DamselRescuingKnight class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public class DamselRescuingKnight implements Knight {

    private RescueDamselQuest quest;

    public DamselRescuingKnight() {
        this.quest = new RescueDamselQuest();
    }

    public void embarkOnQuest() {
        quest.embark();
    }
}
