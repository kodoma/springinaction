package com.springinaction.soundsystem.auto_java_conf.cd;

import org.springframework.stereotype.Component;

/**
 * SgtPeppers class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
@Component
// Эта аннотация идентифицирует этот класс как класс компонента и служит подсказкой для Spring, что для этого класса должен быть создан бин.
// Не нужно явно настраивать бин SgtPeppers; Spring сделает это за вас, потому что этот класс помечен аннотацией @Component.
// Без прямого указания идентификатора бина, его имя по-умолчанию будет sgtPeppers

// @Component ("lonelyHeartsClub")
// Можно указать имя бина

// @Named ( "lonelyHeartsClub")
// Эквивалентно @Component ("lonelyHeartsClub")

public class SgtPeppers implements CompactDisc {

    private String title = "Sgt. Pepper's Lonely Hearts Club Band";

    private String artist = "The Beatles";

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
