package com.springinaction.soundsystem.auto_java_conf.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * CDPlayerConfig class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
@Configuration
// @ComponentScan
// Сканирование на наличие аннотации @Component для автоматического создания и связывания бинов
// По-умолчанию, Spring ищет классы с аннотацией @Component в пакете,
// где объявлен класс с конфигурацией @ComponentScan и в любых под-пакетах

@ComponentScan(basePackages="com.springinaction.soundsystem.auto_java_conf")
// Можно явно указать пакет или несколько пакетов, в которых нужно искать бины

// @ComponentScan(basePackageClasses = CompactDisc.class)
// То же самое, только сканирование через класс
public class CDPlayerConfig {
}
