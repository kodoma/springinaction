package com.springinaction.soundsystem.auto_java_conf.player;

import com.springinaction.soundsystem.auto_java_conf.cd.CompactDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * CDPlayer class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
@Component
public class CDPlayer implements MediaPlayer {

    private CompactDisc cd;

    @Autowired
    // Ищет другие бины в контексте и автоматически заполняет поле

    // @Autowired(required = false)
    // Можно указать поле, как необязательное для автозаполнения

    // @Inject()
    // Аналогично @Autowired
    public CDPlayer(CompactDisc cd) {
        this.cd = cd;
    }

    @Override
    public void play() {
        cd.play();
    }
}
