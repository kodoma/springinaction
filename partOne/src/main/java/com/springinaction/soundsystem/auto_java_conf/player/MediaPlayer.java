package com.springinaction.soundsystem.auto_java_conf.player;

/**
 * MediaPlayer class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
public interface MediaPlayer {

    void play();
}
