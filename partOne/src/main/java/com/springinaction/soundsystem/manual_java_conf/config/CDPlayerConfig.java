package com.springinaction.soundsystem.manual_java_conf.config;

import com.springinaction.soundsystem.manual_java_conf.cd.CompactDisc;
import com.springinaction.soundsystem.manual_java_conf.cd.SgtPeppers;
import com.springinaction.soundsystem.manual_java_conf.player.CDPlayer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * CDPlayerConfig class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
@Configuration
public class CDPlayerConfig {

    // @Bean
    // Аннотация @Bean сообщает Spring, что этот метод вернет объект, который должен быть зарегистрирован
    // как bean-компонент в контексте приложения Spring.
    // По-умолчанию бину будет присвоен идентификатор, совпадающий с возвращаемым значением. В этом случае компонент будет называться compactDisc
    @Bean(name = "lonelyHeartsClubBand")
    public CompactDisc sgtPeppers() {
        return new SgtPeppers();
    }

/*    @Bean
    public CDPlayer cdPlayer() {
        return new CDPlayer(sgtPeppers());
    }*/

/*    @Bean
    public CDPlayer anotherCDPlayer() {
        // Spring перехватывает вызов метода sgtPeppers() и, в отличие от простого Java кода,
        // оба объекта CDPlayer будут ссылаться на один и тот же объект CompactDisc
        return new CDPlayer(sgtPeppers());
    }*/

    // Другой способ внедрения зависимости
    @Bean
    public CDPlayer cdPlayer(CompactDisc compactDisc) {
        // Здесь метод cdPlayer() запрашивает CompactDisc в качестве параметра. Когда Spring вызывает метод cdPlayer()
        // для создания bean-компонента CDPlayer, он автоматически передает CompactDisc в метод конфигурации
        return new CDPlayer(compactDisc);
    }

    // Можно и вовсе отказаться от объявления бина CompactDisc, передав его в качестве параметра, напрямую в объект CDPlayer
/*    @Bean
    public CDPlayer cdPlayer(CompactDisc compactDisc) {
        final CDPlayer cdPlayer = new CDPlayer(compactDisc);
        cdPlayer.setCompactDisc(compactDisc);

        return cdPlayer;
    }*/
}
