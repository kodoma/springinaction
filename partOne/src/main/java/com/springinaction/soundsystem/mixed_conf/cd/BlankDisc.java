package com.springinaction.soundsystem.mixed_conf.cd;

import java.util.List;

/**
 * BlankDisc class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankDisc implements CompactDisc {

    private String title;
    private String artist;
    private List<String> tracks;

    public BlankDisc(String title, String artist, List<String> tracks) {
        this.title = title;
        this.artist = artist;
        this.tracks = tracks;
    }

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
