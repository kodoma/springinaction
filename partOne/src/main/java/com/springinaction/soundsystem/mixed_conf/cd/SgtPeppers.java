package com.springinaction.soundsystem.mixed_conf.cd;

/**
 * SgtPeppers class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
public class SgtPeppers implements CompactDisc {

    private String title = "Sgt. Pepper's Lonely Hearts Club Band";
    private String artist = "The Beatles";

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
