package com.springinaction.soundsystem.mixed_conf.config;

import com.springinaction.soundsystem.mixed_conf.cd.CompactDisc;
import com.springinaction.soundsystem.mixed_conf.cd.SgtPeppers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * CDPlayerConfig class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
@Configuration
public class CDConfig {

    @Bean
    public CompactDisc compactDisc() {
        return new SgtPeppers();
    }
}
