package com.springinaction.soundsystem.mixed_conf.config;

import com.springinaction.soundsystem.mixed_conf.cd.BlankDisc;
import com.springinaction.soundsystem.mixed_conf.player.CDPlayer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * CDPlayerConfig class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
@Configuration
public class CDPlayerConfig {

    @Bean
    public CDPlayer cdPlayer(BlankDisc compactDisc) {
        return new CDPlayer(compactDisc);
    }
}
