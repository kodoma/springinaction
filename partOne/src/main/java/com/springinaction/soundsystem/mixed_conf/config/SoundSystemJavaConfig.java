package com.springinaction.soundsystem.mixed_conf.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * SoundSystemConfig class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
// Объединяем вместе 2 Java конфигурации
@Configuration
@Import({CDPlayerConfig.class, CDConfig.class})
public class SoundSystemJavaConfig {
}
