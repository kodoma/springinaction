package com.springinaction.soundsystem.mixed_conf.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

/**
 * SoundSystemJavaXmlConfig class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
// Объединяем вместе 2 конфигурации - Java и xml
@Configuration
@Import(CDPlayerConfig.class)
@ImportResource("classpath:META-INF/spring/soundsystem/mixed_conf/cd-config.xml")
public class SoundSystemJavaXmlConfig {
}
