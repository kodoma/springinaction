package com.springinaction.soundsystem.mixed_conf.player;

/**
 * MediaPlayer class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
public interface MediaPlayer {

    void play();
}
