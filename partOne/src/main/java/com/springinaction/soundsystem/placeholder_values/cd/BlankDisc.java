package com.springinaction.soundsystem.placeholder_values.cd;

import org.springframework.beans.factory.annotation.Value;

/**
 * BlankDisc class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankDisc implements CompactDisc {

    private String title;
    private String artist;

    public BlankDisc(
            // Для автоматической конфигурации можно использовать аннотацию @Value,
            // чтобы получить доступ к переменной из внешнего источника данных
            @Value("${disc.title}") String title,
            @Value("${disc.artist}") String artist) {
        this.title = title;
        this.artist = artist;
    }

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
