package com.springinaction.soundsystem.placeholder_values.config;

import com.springinaction.soundsystem.placeholder_values.cd.BlankDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

/**
 * ExpressiveConfig class.
 * Created on 17.01.2019.
 * @author Kodoma.
 */
@Configuration
@PropertySource("classpath:/com/soundsystem/app.properties")
// Подключить внешний источник данных, доступных через Environment.
// Этот файл свойств загружается в среду Spring, из которой он может быть восстановлен позже
public class ExpressiveConfig {

    @Autowired
    private Environment env;

    @Bean
    public BlankDisc disc() {
        return new BlankDisc(
                env.getProperty("disc.title"),
                env.getProperty("disc.artist"));
    }

    // Чтобы использовать внешние переменные, нужно объявить бин PropertySourcesPlaceholderConfigurer
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
