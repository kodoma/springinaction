package com.springinaction.soundsystem.spel.cd;

import org.springframework.beans.factory.annotation.Value;

/**
 * BlankDisc class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankDisc implements CompactDisc {

    private String title;
    private String artist;

    public BlankDisc(
            @Value("#{systemProperties['disc.title']}") String title,
            @Value("#{systemProperties['disc.artist']}") String artist) {
        this.title = title;
        this.artist = artist;
    }

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
