package com.springinaction.soundsystem.spel.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * ExpressiveConfig class.
 * Created on 17.01.2019.
 * @author Kodoma.
 */
@Configuration
@ComponentScan(basePackages="com.springinaction.soundsystem.spel")
public class ExpressiveConfig {
}
