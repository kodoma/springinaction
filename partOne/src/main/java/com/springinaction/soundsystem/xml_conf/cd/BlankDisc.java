package com.springinaction.soundsystem.xml_conf.cd;

/**
 * BlankDisc class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankDisc implements CompactDisc {

    private String title;
    private String artist;

    public BlankDisc(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
    }
}
