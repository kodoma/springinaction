package com.springinaction.soundsystem.xml_conf.cd;

import java.util.List;

/**
 * BlankListDisc class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankListDisc implements CompactDisc {

    private String title;
    private String artist;
    private List<String> tracks;

    public BlankListDisc() {
    }

    public BlankListDisc(String title, String artist, List<String> tracks) {
        this.title = title;
        this.artist = artist;
        this.tracks = tracks;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setTracks(List<String> tracks) {
        this.tracks = tracks;
    }

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);

        for (String track : tracks) {
            System.out.println("-Track: " + track);
        }
    }
}
