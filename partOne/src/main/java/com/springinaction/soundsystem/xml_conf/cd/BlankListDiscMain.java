package com.springinaction.soundsystem.xml_conf.cd;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * BlankListDiscMain class.
 * Created on 14.01.2019.
 * @author Kodoma.
 */
public class BlankListDiscMain {

    public static void main(String[] args) {
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/soundsystem/xml_conf/soundsystem.xml");
        final BlankListDisc blankListDisc = (BlankListDisc)context.getBean("listDisc");

        blankListDisc.play();
    }
}
