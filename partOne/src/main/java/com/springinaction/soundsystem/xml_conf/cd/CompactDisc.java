package com.springinaction.soundsystem.xml_conf.cd;

/**
 * CompactDisc class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
public interface CompactDisc {

    void play();
}
