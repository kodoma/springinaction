package com.springinaction.soundsystem.xml_conf.player;

import com.springinaction.soundsystem.xml_conf.cd.CompactDisc;

/**
 * CDPlayer class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
public class CDPlayer implements MediaPlayer {

    private CompactDisc cd;

    public CDPlayer(CompactDisc cd) {
        this.cd = cd;
    }

    @Override
    public void play() {
        cd.play();
    }

    public void setCompactDisc(CompactDisc cd) {
        this.cd = cd;
    }
}
