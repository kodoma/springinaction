package com.springinaction.soundsystem.xml_conf.player;

import com.springinaction.soundsystem.xml_conf.cd.CompactDisc;

/**
 * CDPropertyPlayer class.
 * Created on 15.01.2019.
 * @author Kodoma.
 */
public class CDPropertyPlayer implements MediaPlayer {

    private CompactDisc compactDisc;

    public void setCompactDisc(CompactDisc compactDisc) {
        this.compactDisc = compactDisc;
    }

    public void play() {
        compactDisc.play();
    }
}
