package com.springinaction.aop.concert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * OperaTest class.
 * Created on 18.01.2019.
 * @author Kodoma.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/aop/concert/concert-around.xml")
public class OperaTest {

    @Autowired
    private Performance performance;

    @Test
    public void perform() {
        performance.perform();
    }
}
