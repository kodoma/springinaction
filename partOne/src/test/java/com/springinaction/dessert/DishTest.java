package com.springinaction.dessert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

/**
 * DishTest class.
 * Created on 16.01.2019.
 * @author Kodoma.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DessertManualConfig.class)
public class DishTest {

    @Autowired
    private Dish dish;

    @Test
    public void test() {
        assertNotNull(dish.getDessert());
    }
}
