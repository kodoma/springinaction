package com.springinaction.knights;

import com.springinaction.knights.di.BraveKnight;
import com.springinaction.knights.quests.Quest;
import org.junit.Test;

import static org.mockito.Mockito.*;


/**
 * BraveKnightTest class.
 * Created on 12.01.2019.
 * @author Kodoma.
 */
public class BraveKnightTest {

    @Test
    public void knightShouldEmbarkOnQuest() {
        final Quest mockQuest = mock(Quest.class);
        final Minstrel minstrel = mock(Minstrel.class);

        final BraveKnight knight = new BraveKnight(mockQuest, minstrel);

        knight.embarkOnQuest();

        verify(mockQuest, times(1)).embark();
    }
}
