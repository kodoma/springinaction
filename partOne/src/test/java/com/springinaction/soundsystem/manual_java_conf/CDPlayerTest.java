package com.springinaction.soundsystem.manual_java_conf;

import com.springinaction.soundsystem.manual_java_conf.cd.CompactDisc;
import com.springinaction.soundsystem.manual_java_conf.config.CDPlayerConfig;
import com.springinaction.soundsystem.manual_java_conf.player.MediaPlayer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * BlankListDiscTest class.
 * Created on 13.01.2019.
 * @author Kodoma.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// Для автоматического создания контекста приложения Spring при запуске теста
@ContextConfiguration(classes = CDPlayerConfig.class)
// Создать конфигурацию из класса CDPlayerConfig
public class CDPlayerTest {

    @Rule
    public final StandardOutputStreamLog log = new StandardOutputStreamLog();

    @Autowired
    private MediaPlayer player;

    @Autowired
    private CompactDisc cd;

    @Test
    public void cdShouldNotBeNull() {
        assertNotNull(cd);
    }

    @Test
    public void play() {
        player.play();
        assertEquals("Playing Sgt. Pepper's Lonely Hearts Club Band by The Beatles\r\n", log.getLog());
    }
}
