package spitter.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

/**
 * WebConfig class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
@Configuration
@EnableWebMvc
// Включить Spring MVC
@ComponentScan("spitter")
// Включить компонентное сканирование
public class WebConfig extends WebMvcConfigurerAdapter {

/*    @Bean
    public ViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        // InternalResourceViewResolver обычно используется для JSP

        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        resolver.setExposeContextBeansAsAttributes(true);
        // В итоге имя представления "home" будет выглядеть как "WEB-INF/views/home.jsp"
        // Для передачи тегам JSTL локали и источников сообщений, настроенных в Spring
        // resolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
        return resolver;
    }*/

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public MessageSource messageSource() {
        final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");

        return messageSource;
    }

    @Bean
    public TilesConfigurer tilesConfigurer() {
        // Apache Tiles - механизм разметки
        final TilesConfigurer tiles = new TilesConfigurer();

        tiles.setDefinitions("/WEB-INF/layout/tiles.xml");
        // Можно использовать Regex
        //tiles.setDefinitions("/WEB-INF/layout/tiles.xml");

        tiles.setCheckRefresh(true);

        return tiles;
    }

    @Bean
    public ViewResolver viewResolver() {
        return new TilesViewResolver();
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}