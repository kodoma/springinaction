package spitter.data;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;
import spitter.Spitter;

import java.util.Map;

/**
 * SpitterRepositoryImpl class.
 * Created on 03.02.2019.
 * @author Kodoma.
 */
@Component
public class SpitterRepositoryImpl implements SpitterRepository {

    private static final Map<String, Spitter> SPITTER_CASH = Maps.newHashMapWithExpectedSize(10);

    @Override
    public Spitter save(Spitter spitter) {
        SPITTER_CASH.put(spitter.getUsername(), spitter);
        return spitter;
    }

    @Override
    public Spitter findByUsername(String username) {
        return SPITTER_CASH.get(username);
    }
}
