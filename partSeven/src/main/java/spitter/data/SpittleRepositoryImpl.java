package spitter.data;

import org.springframework.stereotype.Component;
import spitter.Spittle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * SpittleRepositoryImpl class.
 * Created on 24.01.2019.
 * @author Kodoma.
 */
@Component
public class SpittleRepositoryImpl implements SpittleRepository {

    @Override
    public List<Spittle> findSpittles(long max, int count) {
        return createSpittleList(20);
    }

    @Override
    public Spittle findOne(long spittleId) {
        return new Spittle("Spittle " + spittleId, new Date());
    }

    private List<Spittle> createSpittleList(int count) {
        final List<Spittle> spittles = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            spittles.add(new Spittle("Spittle " + i, new Date()));
        }
        return spittles;
    }
}
