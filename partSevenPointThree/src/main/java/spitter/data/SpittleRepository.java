package spitter.data;

import spitter.Spittle;

import java.util.List;

/**
 * SpittleRepository class.
 * Created on 23.01.2019.
 * @author Kodoma.
 */
public interface SpittleRepository {

    List<Spittle> findRecentSpittles();

    List<Spittle> findSpittles(long max, int count);

    Spittle findOne(long id);

    void save(Spittle spittle);
}
