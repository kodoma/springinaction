package spitter.data;

import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;
import spitter.Spittle;
import spitter.exceptions.DuplicateSpittleException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * SpittleRepositoryImpl class.
 * Created on 24.01.2019.
 * @author Kodoma.
 */
@Component
public class SpittleRepositoryImpl implements SpittleRepository {

    private static final Map<Long, Spittle> SPITTLE_CASH = Maps.newHashMap();

    @Override
    public List<Spittle> findRecentSpittles() {
        return null;
    }

    @Override
    public List<Spittle> findSpittles(long max, int count) {
        return createSpittleList(20);
    }

    @Override
    public Spittle findOne(long spittleId) {
        return SPITTLE_CASH.get(spittleId);
    }

    @Override
    public void save(Spittle spittle) {
        final boolean check = SPITTLE_CASH.values().stream().anyMatch(m -> m.getMessage().equals(spittle.getMessage()));
        if (check) {
            throw new DuplicateSpittleException();
        }
        final Long maxId = SPITTLE_CASH.keySet().stream().reduce(Long::max).orElse(0L);
        SPITTLE_CASH.put(maxId + 1, spittle);
    }

    private List<Spittle> createSpittleList(int count) {
        final List<Spittle> spittles = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            spittles.add(new Spittle("Spittle " + i, new Date()));
        }
        return spittles;
    }
}
