package spitter.exceptions;

/**
 * DuplicateSpittleException class.
 * Created on 10.03.2019.
 * @author Kodoma.
 */
public class DuplicateSpittleException extends RuntimeException {
}
