package spitter.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * SpittleNotFoundException class.
 * Created on 10.03.2019.
 * @author Kodoma.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,
                reason="Spittle Not Found")
// Сопоставление SpittleNotFoundException с кодом состояния 404
public class SpittleNotFoundException extends RuntimeException {
}
