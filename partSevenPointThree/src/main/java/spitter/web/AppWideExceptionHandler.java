package spitter.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import spitter.exceptions.DuplicateSpittleException;

/**
 * AppWideExceptionHandler class.
 * Created on 10.03.2019.
 * @author Kodoma.
 */
// Для обработки исключений из всех контроллеров, применяется Advice
@ControllerAdvice
public class AppWideExceptionHandler {

    // Этот метод будет вызываться при возникновении икслючения DuplicateSpittleException
    // в любом контроллере
    @ExceptionHandler(DuplicateSpittleException.class)
    public String duplicateSpittleHandler() {
        return "redirect:/error/duplicate";
    }
}
