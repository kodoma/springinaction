package spitter.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spitter.Spitter;
import spitter.data.SpitterRepository;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * SpitterController class.
 * Created on 03.02.2019.
 * @author Kodoma.
 */
@Controller
@RequestMapping("/spitter")
public class SpitterController {

    private SpitterRepository spitterRepository;

    @Autowired
    public SpitterController(SpitterRepository repository) {
        this.spitterRepository = repository;
    }

    @RequestMapping(value = "register", method = GET)
    public String showRegistrationForm(Model model) {
        model.addAttribute(new Spitter());
        return "registerForm";
    }

/*    @RequestMapping(value = "register", method = POST)
    public String processRegistration(@Valid Spitter spitter, Errors errors) {
        if (errors.hasErrors()) {
            return "registerForm";
        }
        spitterRepository.save(spitter);

        // После перенаправления, аттрибуты запроса не сохраняются,
        // создается новый GET/POST запрос.
        // Атрибуты модели в конечном итоге копируются в запрос как атрибуты запроса и теряются при перенаправлении.
        // Один из вариантов - поместить Spitter в сессию. Сессия длится несколько запросов. Таким образом, вы можете поместить Spitter
        // в сеанс до перенаправления и затем получить его из сеанса после перенаправления.
        // Конечно, вы также несете ответственность за очистку сессии после перенаправления
        // Вместо этого Spring предлагает возможность отправки данных в виде флэш-атрибуты .
        // Флэш-атрибуты по определению переносят данные до следующего запроса, затем они удаляются.
        return "redirect:/spitter/" + spitter.getUsername();
    }*/

    @RequestMapping(value="/register", method=POST)
    public String processRegistration(Spitter spitter, RedirectAttributes model) {
        // RedirectAttributes предлагает все то же, что и Model, плюс несколько методов для установки флэш-атрибутов
        // Перед перенаправлением все флэш-атрибуты копируются в сессию
        // После перенаправление, флэш-атрибуты, хранящиеся в сеансе, перемещаются из сеанса в модель
        spitterRepository.save(spitter);

        model.addAttribute("username", spitter.getUsername());
        model.addFlashAttribute("spitter", spitter);
        // Можно просто model.addFlashAttribute(spitter);
        // тогда ключем в данном случае будет "spitter"

        return "redirect:/spitter/{username}";
    }

    @RequestMapping(value = "/{username}", method = GET)
    public String showSpitterProfile(@PathVariable String username, Model model) {
        // Отправка данных через редирект, через переменные пути и параметры запроса проста,
        // но ограниченна. Она хороша только для отправки простых значений например,
        // String и числовых значений. У нее нет хорошего способа отправить что-нибудь более сложное в URL
        if (!model.containsAttribute("spitter")) {
            model.addAttribute(spitterRepository.findByUsername(username));
        }
        return "profile";
    }
}
