package spitter.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import spitter.Spittle;
import spitter.data.SpittleRepository;
import spitter.exceptions.SpittleNotFoundException;
import spitter.web.forms.SpittleForm;

import java.util.Date;
import java.util.List;

/**
 * SpittleController class.
 * Created on 23.01.2019.
 * @author Kodoma.
 */
@Controller
@RequestMapping("/spittles")
public class SpittleController {

    private SpittleRepository spittleRepository;

    @Autowired
    public SpittleController(SpittleRepository spittleRepository) {
        this.spittleRepository = spittleRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Spittle> spittles(
            @RequestParam(value = "max", defaultValue = "10000000000") long max,
            @RequestParam(value = "count", defaultValue = "20") int count) {

        return spittleRepository.findSpittles(max, count);
    }

    @RequestMapping(value = "/{spittleId}", method = RequestMethod.GET)
    public String spittle(@PathVariable long spittleId, Model model) {
        final Spittle spittle = spittleRepository.findOne(spittleId);

        if (spittle == null) {
            throw new SpittleNotFoundException();
        }
        model.addAttribute(spittle);
        return "spittle";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String saveSpittle(SpittleForm form) {
        spittleRepository.save(
                new Spittle(null, form.getMessage(), new Date(), form.getLongitude(), form.getLatitude())
        );
        return "redirect:/spittles";
    }

    // Вызывется, если брошено исключение DuplicateSpittleException
    // Метод будет обрабатывать исключение, выброшенное из любого метода SpittleController
/*    @ExceptionHandler(DuplicateSpittleException.class)
    public String handleDuplicateSpittle() {
        return "error/duplicate";
    }*/

    // Аналогично
/*    @RequestMapping(method=RequestMethod.POST)
    public String saveSpittle(SpittleForm form) {
        try {
            spittleRepository.save(
                    new Spittle(null, form.getMessage(), new Date(), form.getLongitude(), form.getLatitude())
            );
            return "redirect:/spittles";
        } catch (DuplicateSpittleException e) {
            return "error/duplicate";
        }
    }*/
}


