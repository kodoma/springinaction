package spitter;

import java.util.Date;
import java.util.Objects;

/**
 * Spittle class.
 * Created on 23.01.2019.
 * @author Kodoma.
 */
public class Spittle {

    private final Long id;
    private final String message;
    private final Date time;
    private Double latitude;
    private Double longitude;

    public Spittle(String message, Date time) {
        this(message, time, null, null);
    }

    public Spittle(String message, Date time, Double longitude, Double latitude) {
        this.id = null;
        this.message = message;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Date getTime() {
        return time;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Spittle spittle = (Spittle)o;

        return Objects.equals(id, spittle.id) && Objects.equals(time, spittle.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time);
    }
}
