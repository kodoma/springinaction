package spitter.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * RootConfig class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
@Configuration
@ComponentScan(basePackages = {"spitter"},
               excludeFilters = {@ComponentScan.Filter(
                       type = FilterType.ANNOTATION,
                       value = EnableWebMvc.class)})
public class RootConfig {
}