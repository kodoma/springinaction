package spitter.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletRegistration;

/**
 * SpittrWebAppInitializer class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
public class SpittrWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    // Класс, реализующий AbstractAnnotationConfigDispatcherServletInitializer автоматически обнаруживается при развертывании
    // приложения в контейнере Servlet 3.0 и используется для настройки контекста сервлета

    @Override
    protected String[] getServletMappings() {
        // Определяет один или несколько адресов, куда будет маппиться DispatcherServlet
        // Данный диспетчер будет обрабатывать все запросы, поступающие в приложение
        return new String[]{"/"};
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{RootConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebConfig.class};
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        // Для составных запросов можно включить Multipart конфигурацию

        // registration.setMultipartConfig(new MultipartConfigElement("/tmp/spittr/uploads"));
        // Настраиваем поддержку нескольких частей для временного хранения загруженных файлов
        super.customizeRegistration(registration);
    }
}
