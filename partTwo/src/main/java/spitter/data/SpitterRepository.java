package spitter.data;

import spitter.Spitter;

/**
 * TSpitterRepository class.
 * Created on 03.02.2019.
 * @author Kodoma.
 */
public interface SpitterRepository {

    Spitter save(Spitter spitter);

    Spitter findByUsername(String username);
}
