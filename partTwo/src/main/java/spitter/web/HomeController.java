package spitter.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * HomeController class.
 * Created on 22.01.2019.
 * @author Kodoma.
 */
@Controller
// Объявлен класс контроллером
@RequestMapping({"/", "homepage"})
// Маппинг на уровне класса
public class HomeController {

    @RequestMapping(method = GET)
    // Обработка GET-запросов для "/"
    public String home() {
        return "home";
    }
}