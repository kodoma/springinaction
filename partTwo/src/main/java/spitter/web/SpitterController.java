package spitter.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import spitter.Spitter;
import spitter.data.SpitterRepository;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * SpitterController class.
 * Created on 03.02.2019.
 * @author Kodoma.
 */
@Controller
@RequestMapping("/spitter")
public class SpitterController {

    private SpitterRepository spitterRepository;

    @Autowired
    public SpitterController(SpitterRepository repository) {
        this.spitterRepository = repository;
    }

    @RequestMapping(value = "register", method = GET)
    public String showRegistrationForm(Model model) {
        model.addAttribute(new Spitter());
        return "registerForm";
    }

    @RequestMapping(value = "register", method = POST)
    public String processRegistration(
            // Чтобы применить проверки @NotNull и @Size
            // Даже если проверка не прошла, метод все равно будет вызван
            @Valid Spitter spitter, Errors errors) {

        // Если возникли какие-либо ошибки, они будут в объекте Errors
        if (errors.hasErrors()) {
            return "registerForm";
        }
/*        if (!checkSpitter(spitter)) {
            return "registerForm";
        }*/
        spitterRepository.save(spitter);

        // Когда InternalResourceViewResolver видит префикс "redirect:" в представлении
        // он знает, что его следует интерпретировать как спецификацию перенаправления, а не как имя представления
        return "redirect:/spitter/" + spitter.getUsername();
    }

    @RequestMapping(value = "/{username}", method = GET)
    public String showSpitterProfile(@PathVariable String username, Model model) {
        final Spitter spitter = spitterRepository.findByUsername(username);

        model.addAttribute(spitter);

        return "profile";
    }
}
