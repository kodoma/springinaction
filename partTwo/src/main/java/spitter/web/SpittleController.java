package spitter.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import spitter.Spittle;
import spitter.data.SpittleRepository;

import java.util.List;

/**
 * SpittleController class.
 * Created on 23.01.2019.
 * @author Kodoma.
 */
@Controller
@RequestMapping("/spittles")
public class SpittleController {

    private SpittleRepository spittleRepository;

    @Autowired
    public SpittleController(SpittleRepository spittleRepository) {
        this.spittleRepository = spittleRepository;
    }

/*    @RequestMapping(method = RequestMethod.GET)
    public String spittles(Model model) {
        // Модель - это простая Map-а, которая передается в представление,
        // чтобы данные могли быть переданы клиенту
        model.addAttribute(spittleRepository.findSpittles(Long.MAX_VALUE, 20));
        // Ключем в данном случае будет "spittleList"
        return "spittles";
    }*/

    // Эквивалентно
    // В этом случае имя возвращаемого представления берется из пути запроса "/spittles", исключая первый слеш,
    // т.е. имя представления будет "spittles"

/*    @RequestMapping(method=RequestMethod.GET)
    public List<Spittle> spittles() {
        return spittleRepository.findSpittles(Long.MAX_VALUE, 20));
    }*/

    @RequestMapping(method=RequestMethod.GET)
    public List<Spittle> spittles(
            @RequestParam(value="max", defaultValue="10000000000") long max,
            @RequestParam(value="count", defaultValue="20") int count) {

        return spittleRepository.findSpittles(max, count);
    }

    @RequestMapping(value="/{spittleId}", method=RequestMethod.GET)
    // Определяем переменную запроса
    public String spittle(
            // Поскольку имя placeholder совпадает с именем параметра метода,
            // "spittleId" можно пропустить
            @PathVariable long spittleId,
            // Ссылаемся на переменную запроса
            // @PathVariable("spittleId") long spittleId
            Model model) {

        model.addAttribute(spittleRepository.findOne(spittleId));
        return "spittle";
    }
}
