<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %> --%>
<%-- Для связи модели и представления используется библиотека тегов Spring --%>
<%@ page session="false" %>

<html>
<%--<head>
    <title>Spittr</title>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/style.css"/>" >
</head>--%>

<body>
<h1>Welcome to Spittr</h1>

<%--<a href="<s:url value="/spittles"/>">Spittles</a> |
<a href="<s:url value="/spitter/register"/>">Register</a>--%>

<a href="<c:url value="/spittles" />">Spittles</a> |
<a href="<c:url value="/spitter/register" />">Register</a>

</body>
</html>