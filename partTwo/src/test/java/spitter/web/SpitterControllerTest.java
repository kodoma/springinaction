package spitter.web;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import spitter.Spitter;
import spitter.data.SpitterRepository;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * SpitterControllerTest class.
 * Created on 03.02.2019.
 * @author Kodoma.
 */
public class SpitterControllerTest {

    @Test
    public void shouldShowRegistration() throws Exception {
        final SpitterRepository mockRepository = mock(SpitterRepository.class);
        final SpitterController controller = new SpitterController(mockRepository);
        final MockMvc mockMvc = standaloneSetup(controller).build();

        mockMvc.perform(get("/spitter/register"))
               .andExpect(view().name("registerForm"));
    }

    @Test
    public void shouldProcessRegistration() throws Exception {
        final SpitterRepository mockRepository = mock(SpitterRepository.class);
        final Spitter unsaved = new Spitter("jbauer", "24hours", "Jack", "Bauer");
        final Spitter saved = new Spitter(24L, "jbauer", "24hours", "Jack", "Bauer");
        final SpitterController controller = new SpitterController(mockRepository);
        final MockMvc mockMvc = standaloneSetup(controller).build();

        when(mockRepository.save(unsaved)).thenReturn(saved);
        mockMvc.perform(post("/spitter/register")
                                .param("firstName", "Jack")
                                .param("lastName", "Bauer")
                                .param("username", "jbauer")
                                .param("password", "24hours"))
               .andExpect(redirectedUrl("/spitter/jbauer"));

        verify(mockRepository, atLeastOnce()).save(unsaved);
    }
}
